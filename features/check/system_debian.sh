#!/usr/bin/env bash
##
# BASH script that checks the following:
#   - Memory usage
#   - CPU load
#   - Number of TCP connections 
#   - Kernel version
##

server_name=$(hostname)
printf "\\n\\n"
echo "Informations about ${server_name}"
printf "\\n"
lsb_release -d
echo "Kernel version: $(uname -r)"
printf "\\n"

function memory_check() {
	echo "Memory usage: "
	free -h
	printf "\\n\\n"
}


function cpu_check() {
	printf "Current CPU load: $(uptime)\\n"
}

function tcp_check() {
	printf "Total TCP connections: $(cat  /proc/net/tcp | wc -l)\\n"
}


function all_checks() {
	memory_check
	cpu_check
	tcp_check
}

all_checks