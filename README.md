# larbin

CLI Linux desktop and server check/update automation for small inventories.

## Installation

- add the dependencies `sudo apt install git keepassxc openssh-client python3-pip -y`
- install keepass-cli `pip3 install keepass-cli`
- inside the folder of your choice, clone the repository `git clone git@framagit.org:colibris/larbin.git`
- go to your repository folder `cd larbin`
- symlink to your local bin folder `sudo ln -s $(pwd)/larbin.sh /usr/local/bin/larbin` (we assume /usr/local/bin is in your PATH)
- check if larbin can be launched `larbin` (should result in an error about .env file missing)
- copy example .env `cp .env.example .env`
- edit .env `vi .env` and enter your informations

## Usage

`larbin inventory`  Display all machines found in your inventory  
`larbin check <folder or machine>`  Display useful informations about servers in inventory  
`larbin clean <folder or machine>`  Clean machines in inventory  
`larbin upgrade <folder or machine>`  Upgrade machines in inventory  

Options: `-y` `--yes`  Answers yes to every prompt.

## Roadmap

- add logging system
- add install features
- improve security features
