#!/usr/bin/env bash
#
# larbin
# Bash utility that automates adminsys tasks on a folder based server inventory
#
# by Florian Schmitt mrflos@lilo.org
# Based on Bash Boilerplate: https://github.com/xwmx/bash-boilerplate by William Melody hi@williammelody.com

# Short form: set -u
set -o nounset

# Exit immediately if a pipeline returns non-zero
#
# Short form: set -e
set -o errexit

# Allow the above trap be inherited by all functions in the script.
#
# Short form: set -E
set -o errtrace

# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
set -o pipefail

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
IFS=$'\n\t'

###############################################################################
# Environment
###############################################################################

_ME="$(basename "${0}")" # This program's basename.
_DIR="$(dirname $(realpath "${0}"))" # This program's directory.
_INVENTORY_DIR=$_DIR"/inventory"  # This program's inventory directory.

# colors
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
WHITE='\033[0;37m'
RESET='\033[0m'
#echo -e "The Italian flag colors are ${GREEN}GREEN${RESET}, ${WHITE}WHITE${RESET}, and ${RED}RED${RESET}."

###############################################################################
# Debug
###############################################################################

# _debug()
#
# Usage:
#   _debug <command> <options>...
#
# Description:
#   Execute a command and print to standard error. The command is expected to
#   print a message and should typically be either `echo`, `printf`, or `cat`.
#
# Example:
#   _debug printf "Debug info. Variable: %s\\n" "$0"
__DEBUG_COUNTER=0
_debug() {
  if ((${_USE_DEBUG:-0}))
  then
    __DEBUG_COUNTER=$((__DEBUG_COUNTER+1))
    {
      # Prefix debug message with "bug (U+1F41B)"
      printf "🐛  %s " "${__DEBUG_COUNTER}"
      "${@}"
      printf "―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――\\n"
    } 1>&2
  fi
}

###############################################################################
# Error Messages
###############################################################################

# _exit_1()
#
# Usage:
#   _exit_1 <command>
#
# Description:
#   Exit with status 1 after executing the specified command with output
#   redirected to standard error. The command is expected to print a message
#   and should typically be either `echo`, `printf`, or `cat`.
_exit_1() {
  {
    printf "%s " "$(tput setaf 1)!$(tput sgr0)"
    "${@}"
  } 1>&2
  exit 1
}

# _warn()
#
# Usage:
#   _warn <command>
#
# Description:
#   Print the specified command with output redirected to standard error.
#   The command is expected to print a message and should typically be either
#   `echo`, `printf`, or `cat`.
_warn() {
  {
    printf "%s " "$(tput setaf 1)!$(tput sgr0)"
    "${@}"
  } 1>&2
}

###############################################################################
# Help
###############################################################################

# _print_help()
#
# Usage:
#   _print_help
#
# Description:
#   Print the program help information.
_print_help() {
  echo -e "$(
  cat <<HEREDOC
${CYAN}         _____${RESET}
${CYAN}       .'/L|__\`.   ${RESET}      🤖 ${CYAN}larbin${RESET}
${CYAN}      / =[_]O|\` \\ ${RESET}       
${CYAN}      |"+_____":|   ${RESET}     Bash utility that automates adminsys tasks on a folder based server inventory 
${CYAN}    __:='|____\`-:__${RESET}      
${CYAN}   ||[] ||====| []||${RESET}     Usage:
${CYAN}   ||[] | |=| | []||${RESET}       ${GREEN}${_ME} inventory${RESET}  Display all server found in your inventory
${CYAN}   |:||_|=|U| |_||:|${RESET}       ${GREEN}${_ME} check <folder or server>${RESET}  Display useful informations about servers in inventory
${CYAN}   |:|||]_=_ =[_||:|${RESET}       ${GREEN}${_ME} clean <folder or server>${RESET}  Clean servers in inventory
${CYAN}   | |||] [_][]C|| |${RESET}       ${GREEN}${_ME} upgrade <folder or server>${RESET}  Upgrade servers in inventory
${CYAN}   | ||-'"""""\`-|| |${RESET}     
${CYAN}   /|\\\\_\\_|_|_/_//|\\ ${RESET}     Options:
${CYAN}  |___|   /|\\  |___|${RESET}       ${GREEN}-y --yes${RESET}  Answers yes to every prompt.  
${CYAN}  \`---'  |___| \`---'${RESET}    
${CYAN}         \`---'${RESET}


HEREDOC
)"
}

###############################################################################
# Options
#
# NOTE: The `getops` builtin command only parses short options and BSD `getopt`
# does not support long arguments (GNU `getopt` does), so the most portable
# and clear way to parse options is often to just use a `while` loop.
#
# For a pure bash `getopt` function, try pure-getopt:
#   https://github.com/agriffis/pure-getopt
#
# More info:
#   http://wiki.bash-hackers.org/scripting/posparams
#   http://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html
#   http://stackoverflow.com/a/14203146
#   http://stackoverflow.com/a/7948533
#   https://stackoverflow.com/a/12026302
#   https://stackoverflow.com/a/402410
###############################################################################

# Parse Options ###############################################################

# Initialize program option variables.
_PRINT_HELP=0
_USE_DEBUG=0

# Initialize additional expected option variables.
_INVENTORY=
_ACTION=
_ACTION_OPTION=
_YES=

# __get_option_value()
#
# Usage:
#   __get_option_value <option> <value>
#
# Description:
#  Given a flag (e.g., -e | --example) return the value or exit 1 if value
#  is blank or appears to be another option.
__get_option_value() {
  local __arg="${1:-}"
  local __val="${2:-}"

  if [[ -n "${__val:-}" ]] && [[ ! "${__val:-}" =~ ^- ]]
  then
    printf "%s\\n" "${__val}"
  else
    _exit_1 printf "%s requires a valid argument.\\n" "${__arg}"
  fi
}

while ((${#}))
do
  __arg="${1:-}"
  __val="${2:-}"

  case "${__arg}" in
    -h|--help)
      _PRINT_HELP=1
      ;;
    -y|--yes)
      _YES="-y"
      ;;
    --debug)
      _USE_DEBUG=1
      ;;
    inventory)
      _INVENTORY=1
      ;;
    check|clean|upgrade)
      _ACTION=${__arg}
      _ACTION_OPTION="$(__get_option_value "${__arg}" "${__val:-}")"
      ;;
    --endopts)
      # Terminate option parsing.
      break
      ;;
    -*)
      _exit_1 printf "Unexpected option: %s\\n" "${__arg}"
      ;;
  esac

  shift
done

###############################################################################
# Program Functions
###############################################################################

_get_keepass_password() {
  [[ -z "${KEEPASSPASSWORD+set}" ]] && read -p 'Keepass Password : ' -s KEEPASSPASSWORD
  echo "$(echo $KEEPASSPASSWORD | keepassxc-cli show $KEEPASSDB $yaml_keepassentryname -a Password)"
}

_check_server_yaml() {
  unset ${!yaml_*}
  local yaml_file="$1"
  source $_DIR/libs/yaml.sh
  create_variables $yaml_file "yaml_"

  # test if variables where initialised
  #set -o posix ; set
  if [[ -z "${yaml_sshserveralias+set}" ]]
  then
    _exit_1 printf "Value for sshserveralias is required in $yaml_file \\n"
  fi
  if [[ -z "${yaml_keepassentryname+set}" ]]
  then
    _exit_1 printf "Value for keepassentryname is required in $yaml_file \\n"
  fi
  if [[ -z "${yaml_features_system+set}" ]]
  then
    _exit_1 printf "Value for 
    features: 
     - system
is required in $yaml_file \\n"
  fi
}

_launch_scripts() {
  local action="$1"
  pass=$(_get_keepass_password)
  # get all variables starting yaml_features_ to find scripts that matches
  set -o posix ; features=$(set | grep "^yaml_features_" | grep -v "'")

  for var in $features
  do
    v=$(echo "$var" | tr "=" "\\n" | awk 'FNR <= 1')
    v=${v/yaml_features_/}_${!v}
    if [ -f $_DIR/features/$action/$v.sh ]
    then
      destscript="/home/admin/$v.sh"
      scp $_DIR/features/$action/$v.sh $yaml_sshserveralias:/home/admin > /dev/null
      ssh -t $yaml_sshserveralias <<EOF 
    echo "$pass" | sudo -S bash $destscript $_YES
    rm $destscript
EOF
    fi
  done
}

_print_info() {
  local action="$1"
  local msg="$2"
  if [ "$action" == "upgrade" ]
  then
    printf "\\n♻️  Upgrade"
  elif [ "$action" == "check" ]
  then
    printf "\\n☑️  Check"
  elif [ "$action" == "clean" ]
  then
    printf "\\n🧹  Clean"
  fi
  printf " $msg \\n"
}

_check_inventory_option() {
  local action="$1"
  local option="$2"
  if [[ -d "$_INVENTORY_DIR/${option}" ]]
  then
    _print_info $action "all servers from folder ${option}"
    for server in `find $_INVENTORY_DIR/$option -type f -name "*.yaml" | sort -n`; do
      servername=$( echo $server | sed "s,$_INVENTORY_DIR/$option/,,g" | sed "s/.yaml//" )
      printf "\\n$BLUE====================$servername====================$RESET\\n"
      _check_server_yaml $server
      _launch_scripts $action $_YES
      printf "$BLUE===================================================$RESET\\n"
    done
  else
    SERVER_YAML="$_INVENTORY_DIR/${option}.yaml"
    if [[ -f $SERVER_YAML ]]
    then
       _print_info $action "server described in ${option}.yaml"
      printf "$BLUE====================$option====================$RESET\\n"
      _check_server_yaml $SERVER_YAML
      _launch_scripts $action $_YES
      printf "$BLUE===================================================$RESET\\n"
    else
      _warn printf "${RED}Required folder or server not found. $_INVENTORY_DIR/${option} is not an existing folder or server.${RESET}\\nExample : ${GREEN}larbin check <folder or server>${RESET}\\nFind folder and server names with ${GREEN}larbin inventory${RESET}\\n"
    fi
  fi
}
_print_inventory() {
  printf "\\n🗂  Your inventory folders : \\n${GREEN}"
  find $_INVENTORY_DIR -type d -depth 1 | sed "s,$_INVENTORY_DIR/,,g"
  printf "${RESET}\\n💻  Your servers : \\n${GREEN}"
  find $_INVENTORY_DIR -type f -name "*.yaml" | sed "s,$_INVENTORY_DIR/,,g" | sed "s/.yaml//"
  printf "${RESET}\\n"
}

###############################################################################
# Main
###############################################################################

# _main()
#
# Usage:
#   _main [<options>] [<arguments>]
#
# Description:
#   Entry point for the program, handling basic option parsing and dispatching.
_main() {
  if ((_PRINT_HELP))
  then
    _print_help
  else
    _debug printf ">> Start the main program...\\n"
    if [ -f $_DIR"/.env" ]
    then
      source $_DIR"/.env"
    else
      _exit_1 printf ".env file doesn't exist\\n\\n"
    fi
    if [[ -n "${_ACTION}" ]]
    then
      _check_inventory_option ${_ACTION} ${_ACTION_OPTION}
    elif [[ -n "${_INVENTORY}" ]]
    then
      _print_inventory
    else
      _print_help
    fi
  fi
}

# Call `_main` after everything has been defined.
_main "$@"
